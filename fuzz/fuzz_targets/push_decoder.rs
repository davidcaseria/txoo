#![cfg_attr(not(feature = "repro"), no_main)]
#![allow(dead_code)]

use bitcoin::consensus::{Encodable, ReadExt};
use bitcoin::hashes::Hash;
use bitcoin::{
    Block, BlockHash, BlockHeader, OutPoint, PackedLockTime, Script, Transaction, TxIn,
    TxMerkleNode, TxOut, Txid, Witness,
};
use libfuzzer_sys::fuzz_target;
use push_decoder::{test_util::MockListener, BlockDecoder};
use std::io::Cursor;

fn run(data: &[u8]) {
    let mut reader = Cursor::new(data);
    let capacity = reader.read_u8().unwrap_or(0).max(100);
    let mut listener = MockListener::new();
    let mut decoder = BlockDecoder::new_with_capacity(capacity as usize, 64);
    // create a block with one transaction
    let mut txs = vec![];
    let num_txs = reader.read_u8().unwrap_or(0).max(1);
    let do_witness = reader.read_bool().unwrap_or(false);

    for _ in 0..num_txs {
        let mut num_in = reader.read_u8().unwrap_or(0).max(1) as usize;
        // expand the range
        if num_in >= 128 {
            num_in *= 3;
        }
        let mut num_out = reader.read_u8().unwrap_or(0).max(1) as usize;
        // expand the range
        if num_out >= 128 {
            num_out *= 3;
        }

        let tx = Transaction {
            version: 1,
            lock_time: PackedLockTime(0),
            input: (0..num_in)
                .map(|inp| {
                    let witness: Vec<Vec<u8>> = if do_witness {
                        // do 1-3 witness items
                        let num_witness = reader.read_u8().unwrap_or(0).max(1).min(3) as usize;
                        (0..num_witness)
                            .map(|_| {
                                let len = reader.read_u8().unwrap_or(0).min(80) as usize;
                                (0..len).map(|_| 0x99u8).collect::<Vec<_>>()
                            })
                            .collect()
                    } else {
                        vec![]
                    };
                    let len = read_script_len(&mut reader);
                    let script = Script::from((0..len).map(|_| 0x77u8).collect::<Vec<_>>());
                    TxIn {
                        previous_output: OutPoint {
                            txid: Txid::from_slice(&[0x33u8; 32]).unwrap(),
                            vout: inp as u32,
                        },
                        script_sig: script,
                        sequence: Default::default(),
                        witness: Witness::from_vec(witness),
                    }
                })
                .collect(),
            output: (0..num_out)
                .map(|out| {
                    let len = read_script_len(&mut reader);
                    let script = Script::from((0..len).map(|_| 0x88u8).collect::<Vec<_>>());
                    TxOut { value: out as u64, script_pubkey: script }
                })
                .collect(),
        };
        txs.push(tx);
    }

    let tx_ids = txs.iter().map(|tx| tx.txid()).collect::<Vec<_>>();
    let mut block = Block {
        header: BlockHeader {
            version: 1,
            prev_blockhash: BlockHash::all_zeros(),
            merkle_root: TxMerkleNode::all_zeros(),
            time: 0,
            bits: 0,
            nonce: 0,
        },
        txdata: txs,
    };
    block.header.merkle_root = block.compute_merkle_root().unwrap();
    let block_hash = block.block_hash();
    let mut block_bytes = Vec::new();
    block.consensus_encode(&mut block_bytes).unwrap();
    #[cfg(feature = "repro")]
    println!("block: {}\n{:#?}", hex::encode(&block_bytes), block);
    decoder.decode_next(&block_bytes, &mut listener).unwrap();
    decoder.finish().unwrap();
    assert_eq!(listener.block_headers.len(), 1);
    assert_eq!(listener.transaction_ids, tx_ids);
    assert_eq!(listener.block_headers[0].block_hash(), block_hash);
}

fn read_script_len(reader: &mut Cursor<&[u8]>) -> usize {
    let mut len = reader.read_u8().unwrap_or(0) as usize;
    if len >= 128 {
        len *= 39; // expand the range - max is 9945
    }
    len
}

fuzz_target!(|data: &[u8]| {
    run(data);
});

// look for Base64: in the crash output from cargo-fuzz
const INPUT: &str = "//8AAAAu";

// Reproduce a crash from cargo-fuzz
#[cfg(feature = "repro")]
fn main() {
    use base64::Engine;

    setup_logging();
    let data = base64::engine::general_purpose::STANDARD.decode(INPUT).unwrap();
    run(&data);
}

fn setup_logging() {
    let mut builder = env_logger::Builder::new();
    builder.filter(None, log::LevelFilter::Trace);

    if let Ok(rust_log) = std::env::var("RUST_LOG") {
        builder.parse_filters(&rust_log);
    }

    builder.init();
}
