use crate::log_util::setup_logging;
use bitcoin::Network;
use bitcoind_client::bitcoind_client::bitcoind_client_from_url;
use bitcoind_client::{default_bitcoin_rpc_url, BitcoindClient, BlockSource};
use clap::Parser;
use std::cmp::max;
use std::env::current_dir;
use std::process::exit;
use txoo::bitcoin;
use txoo::source::{source_from_url, Source};
use url::Url;

mod log_util;

#[derive(clap::Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Opts {
    #[clap(short, long, default_value = "bitcoin")]
    network: Network,
    #[clap(short, long, help = "bitcoind RPC URL, must have http(s) schema")]
    rpc: Option<String>,
    #[clap(
        short,
        long,
        help = "data directory path or source URL.  if not set, will use ~/.txoo/<network>/."
    )]
    source: Option<String>,
    #[clap(subcommand)]
    subcommand: SubCommand,
}

#[derive(clap::Parser, Debug)]
enum SubCommand {
    // check the attestation source
    #[clap(author, version, about, long_about = None)]
    Check,
}

#[tokio::main]
async fn main() {
    setup_logging("info");
    let opts: Opts = Opts::parse();
    let network = opts.network.clone();
    let rpc_s = default_bitcoin_rpc_url(opts.rpc.clone(), network);
    let rpc = Url::parse(&rpc_s).expect("rpc url");

    let source_url = match opts.source {
        Some(s) => Url::from_directory_path(current_dir().unwrap())
            .unwrap()
            .join(&s)
            .unwrap(),
        None => {
            let home = dirs::home_dir().expect("cannot get cookie file if HOME is not set");
            let url =
                Url::from_directory_path(home.join(".txoo").join(network.to_string())).unwrap();
            url
        }
    };

    let source: Box<dyn Source> = source_from_url(source_url).await.expect("source");
    let client = bitcoind_client_from_url(rpc, network).await;
    match opts.subcommand {
        SubCommand::Check => {
            run_check(client, source).await;
        }
    }
}

async fn run_check(client: BitcoindClient, source: Box<dyn Source>) {
    let info = client.get_blockchain_info().await.unwrap();
    let height = info.latest_height as u32;
    // start 256 blocks in the past, or at 1 if height is too low
    let num_blocks = 256;
    let mut height = max(257, height) - num_blocks;
    let mut errors = 0u32;
    loop {
        let hash_opt = client.get_block_hash(height).await.unwrap();
        if let Some(hash) = hash_opt {
            let block = client.get_block(&hash).await.unwrap();
            match source.get(height, &block).await {
                Ok(_) => {}
                Err(e) => {
                    println!("{} error: {:?}", height, e);
                    errors += 1;
                }
            }
            height += 1;
            if height % 100 == 0 {
                println!("{}: {} errors", height, errors);
            }
        } else {
            break;
        }
    }

    let setup = source.oracle_setup().await;
    println!(
        "checked {} blocks for oracle {} network {}",
        num_blocks, setup.public_key, setup.network
    );
    if errors > 0 {
        println!("{} errors", errors);
        exit(1);
    }
}
